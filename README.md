
<!-- 
## Available Scripts

In the project directory, you can run:

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`
Launches the test runner in the interactive watch mode.\ -->


# **Candidate subject**

### **First step**

Some data about books is available on  [`https://flit-open-library.s3.eu-west-3.amazonaws.com/books2.json`](https://flit-open-library.s3.eu-west-3.amazonaws.com/books2.json).

We would like to retrieve the `date` and the `title` of all the books

We want to display this data for all the book with the following format: `<date> : <title>`

The UI must be really simple, `<div></div>` are enough

### **Second  step**

The date is very long. We want to format it as `October 8, 1486`.

We also want to sort the books by dates and remove the invalid dates.

### Third  **step**

We now only want to display the year like `<year> : <title>`  so that it will be easier to read when we all the books :

 - [`https://flit-open-library.s3.eu-west-3.amazonaws.com/books1.json`](https://flit-open-library.s3.eu-west-3.amazonaws.com/books1.json)
 - [`https://flit-open-library.s3.eu-west-3.amazonaws.com/books2.json`](https://flit-open-library.s3.eu-west-3.amazonaws.com/books2.json)
 - [`https://flit-open-library.s3.eu-west-3.amazonaws.com/books3.json`](https://flit-open-library.s3.eu-west-3.amazonaws.com/books3.json)

### Fourth  **step**

We would like to retrieve the number of books per year.

The data will look like : `<year> : <number_of_books>`

### Fifth  **step**

We would like to display the number of books per year for each year between 1800 and 1899 

The data will still look like : `<year> : <number_of_books>`