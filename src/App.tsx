import { useState } from 'react';
import './App.css';

interface BookProps {
  date: string;
  title: string;
}

export const App = () => {
  const name: string = 'Display books';

  const [displayBooks, setDisplayBooks] = useState<BookProps[] | []>([]);

  const handleOnClick = async () => {
    console.log("No books retrieved")
  };

  return (
    <div>
      <h1>📚 Welcome at Flit's books library 📕</h1>
      <ul>
        <button onClick={() => handleOnClick()}>
          {name}
        </button>
      </ul>
    </div>
  );
};

export default App;
